variable "subscription_id"{}
variable  "client_id"{}
variable "client_secret"{}
variable "tenant_id"{}
variable  "resource_group"{}
variable "virtual_network" {}
variable "subnet" {}
variable "Security" {}
variable "Storage" {}
variable "disk" {type= "list"}
variable "computer_name" {}
variable "ssh_key"{}
variable "user_name"{}
variable "location"{}
variable "private_ip" {type = "list"}
variable "network_interface"{type = "list"}
variable "vm"{type= "list"}
